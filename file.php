<?php 

include __DIR__ . '/config.php';
include __DIR__ . '/functions.php';

$file = $_GET['file'];
$contents = array();	
$rel_file = $root .'/' . $file;
$type = '';

if(file_exists($rel_file)){
	$type = getFileType($file);

	$handle = fopen($rel_file, 'r');
	$contents = htmlentities(fread($handle, filesize($rel_file)));

}


?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, user-scalable=no">
	<title>Local Host</title>
	<link rel="stylesheet" type="text/css" href="/view/fonts/css/localhost.css">
	<link rel="stylesheet" type="text/css" href="/view/css/style.css">
	<link rel="stylesheet" type="text/css" href="/view/css/inspector/monokai-sublime.css">
	<!-- <link rel="stylesheet" type="text/css" href="/view/css/inspector/default.min.css"> -->
	<style>
		/* for block of numbers */
		td.hljs-ln-numbers {
			-webkit-touch-callout: none !important;
			-webkit-user-select: none !important;
			-khtml-user-select: none !important;
			-moz-user-select: none !important;
			-ms-user-select: none !important;
			user-select: none !important;

			text-align: center !important;
			color: #ccc !important;
			border-right: 1px solid #CCC !important;
			vertical-align: top !important;
			padding-right: 5px !important;

			/* your custom style here */
		}

		/* for block of code */
		td.hljs-ln-code {
			padding-left: 10px !important;
		}
	</style>

</head>
<body>
	<?php $line_n = 1; ?>
	<div id="file">
		<h1><a href="/"><?php echo $root ?></a><a href="<?php echo $file; ?>"><?php echo $file; ?></a></h1>
		<pre class="contents"><code class="<?php echo $type; ?>"><?php echo $contents; ?></code></pre>
	</div>

<script src="/view/js/inspector/highlight.min.js"></script>
<script src="/view/js/inspector/highlightjs-line-numbers.js"></script>
<script>
	hljs.initHighlightingOnLoad();
	hljs.initLineNumbersOnLoad();
</script>
</body>

	<!-- <?php if (!empty($type)): ?> <script src="/view/js/inspector/<?php echo $type; ?>.js"></script> <?php endif ?> -->
</html>