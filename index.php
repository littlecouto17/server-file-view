<?php 

include __DIR__ . '/config.php';
include __DIR__ . '/functions.php';

ini_set('display_errors', 1);
error_reporting(E_ALL);

$folders = folders();

?>

<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, user-scalable=no">
	<title>Local Host</title>
	<link rel="stylesheet" type="text/css" href="/view/fonts/css/localhost.css">
	<link rel="stylesheet" type="text/css" href="/view/css/style.css">
</head>
<body>
	<!-- <h1>Projetos</h1> -->
	<header id="main-header">

		<h1><?php echo $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']; ?></h1>
	
		<div class="change-type-view">
			<span class="type-view">Tipo de visualização</span>
			<ul>
				<li data-type="list">Lista</li>
				<li data-type="side-by-side" class="active">Lado a lado</li>
				<li data-type="thumbs">Miniaturas</li>
			</ul>
			
		</div>

	</header>

	<div id="view">
		<?php echo $folders; ?>
	</div>

	<script src="/view/js/list.js"></script>
</body>
</html>