window.addEventListener('load', function(){

	var lines = document.querySelectorAll('.line-content');
	var keywords = ['var', 'function', 'while', 'for', 'in', 'this', 'public', 'finally', 'if', 'else', 'eval', 'true', 'false', 'typeof', 'try', 'catch', 'delete', 'const', 'return', 'switch', 'throw', 'void', 'default', 'class', 'arguments'];
	var keyRegex = eval('/('+ keywords.join('|') + ')/gi');
	var varArray = /[\w_]+\.[\w_]+/;

	if(lines.length > 0){

		lines.forEach(function(line){

			var content = line.querySelector('.line');
			if(content !== null){

				var html = content.innerHTML;

				// if(indexOfStr(keywords, html) > -1){

				// 	html = html.replace(keyRegex, function(v){
				// 		return '<span class="keyword">' + v + '</span>';
				// 	});

				// }

				var words = html.split(/\s+/);
				if(words.length > 0){

					words.forEach(function(word, i){

						if(indexOfStr(keywords, word)){

							if(varArray.test(word)){
								word = word.replace(varArray, function(v){
									return '<span class="var-array">'+v+'</span>';
								});
								console.log(word)
							}

						}

						words[i] = word;

					});
					console.log(words.join(' '));
				} 

				html = words.join(' ');


				content.innerHTML = html;

			}

		});

	}

}, false);

var indexOfStr = function(array, string){

	for(var i in array){

		var index = string.indexOf(array[i]);

		if(index > -1) 
			return index;

	};


	return -1;
}