
window.onload = function(){

	var openLinks = document.querySelectorAll('.list-files li .open-list');

	openLinks.forEach(function(openLink){

		if(!openLink){
			return;
		}

		openLink.onclick = openClick;

	});

	var favoriteLinks = document.querySelectorAll('.list-files li .favorite');

	favoriteLinks.forEach(function(link){

		if(link instanceof Node){

			link.onclick = doFavorited;
			var icon = link.childNodes[0];
			var href = link.nextElementSibling;

			var favorited_JSON = window.localStorage.getItem('favorited_folders');

			if(favorited_JSON == null){
				favorited_JSON = '[]';
			}

			var favorited = JSON.parse(favorited_JSON);
			var index = favorited.indexOf(href.href);

			if(index === -1){
				link.className = link.className.replace('favorited', '').trim();
				link.parentElement.className = link.parentElement.className.replace('favorited', '').trim();
				classe = 'icon-star-empty'
			} else {
				link.className += ' favorited';
				link.parentElement.className += ' favorited';
				classe = 'icon-star'
			}

			icon.className = classe;

			console.log(favorited_JSON);

		}

	});

	var activeTypeClassName = 'active';

	var changeTypeView = document.querySelector('#main-header .change-type-view');
	var viewFiles = document.querySelector('#view .list-files');

	var viewsType = changeTypeView.querySelectorAll('ul li');
	
	var activeTypeView = changeTypeView.querySelector('.' + activeTypeClassName);

	if(viewsType.length && window.localStorage.getItem('type-view') !== null){


		activeTypeView.className = activeTypeView.className.replace(activeTypeClassName, '');

		var typeView = window.localStorage.getItem('type-view');

		var elementTypeView = changeTypeView.querySelector('ul [data-type="' + typeView + '"]')
		elementTypeView.className += 'active';
		viewFiles.className += ' ' + typeView;

	} else {

		viewFiles.className += ' side-by-side';

	}

	viewsType.forEach(function(viewType){

		viewType.onclick = changeTypeViewTo;

	});

};


var doFavorited = function (e) {

	e.preventDefault();

	var icon = this.childNodes[0];
	var link = this.nextElementSibling;

	var classe = '';


	var favorited_JSON = window.localStorage.getItem('favorited_folders');

	if(favorited_JSON == null){
		favorited_JSON = '[]';
	}
	
	var favorited = JSON.parse(favorited_JSON);
	var index = favorited.indexOf(link.href);




	if(index > -1){
		this.className = this.className.replace('favorited', '').trim();
		this.parentElement.className = this.className.replace('favorited', '').trim();
		classe = 'icon-star-empty'
		favorited.splice(index, 1);
	} else {
		this.className += ' favorited';
		this.parentElement.className += ' favorited';
		classe = 'icon-star'
		favorited.push(link.href);
	}

	icon.className = classe;

	window.localStorage.setItem('favorited_folders', JSON.stringify(favorited));

};

var changeTypeViewTo = function (e) {
	
	if (typeof this.dataset === 'undefined' || this.dataset.type === 'undefined' || this.className.indexOf('active') > -1){
		return;
	}

	var changeTypeView = this.parentElement;
	var viewFiles = document.querySelector('#view .list-files');

	var typeChoosed = this.dataset.type;
	var activeTypeView = changeTypeView.querySelector('.active');

	viewFiles.className = viewFiles.className.replace(' ' + activeTypeView.dataset.type, '');

	activeTypeView.className = activeTypeView.className.replace('active', '');
	this.className += 'active';

	window.localStorage.setItem('type-view', this.dataset.type);

	viewFiles.className += ' ' + typeChoosed;

}

var openClick = function(e){

	e.stopPropagation();

	var li = this.parentElement;

	if(!li){
		return;
	}

	
	var classe = li.className;

	if(classe.indexOf('open') > -1){

		li.className = classe.replace(' open', '');

	} else {

		var opened = document.querySelector('.list-files li.open');

		if(opened!==null){

			opened.className = classe.replace(' open', '');

		}

		li.className += ' open';

	}

};

function isDescendant(parent, child) {
	if(child!==null&&parent!==null){
		var node = child.parentNode;
		while (node != null) {

			if (node == parent) 
				return true;

			if (node == null)
				return false

			node = node.parentNode;
		}
	}
	return false;
}

document.onclick = function(e){

	var target = e.target;

	if(isDescendant(document.querySelector('li.open'), target)){
		return;
	}

	var opened = document.querySelectorAll('.list-files li.open');

	if(!opened){
		return;
	}

	opened.forEach(function(li){

		var classe = li.className;

		if(classe.indexOf('open') > -1){

			li.className = classe.replace(' open', '');

		}

	})
};
