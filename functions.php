<?php 

function folders($path = false, $only_folders = false, $only_files = false, $folder_deep = 0) {

	if ($folder_deep > 2){
		return;
	}
	
	global $root;
	
	if(!$path){
		$path = $root;
	}

	if(!is_dir($path)){

		trigger_error("the \"$path\" is not a valid directory", E_USER_WARNING);
		return false;
		
	}

	if(is_dir($path . $_SERVER['REQUEST_URI'])){
		$path .= $_SERVER['REQUEST_URI'];
	}

	$folders = scandir($path);
	$files = array();

	$skip = array('.', '..', 'fonts', 'view');
	$files_n_folders = array_filter($folders, function($file) use ($skip){

		return !in_array($file, $skip); 

	});

	$relpath = str_replace($root, '', $path);

	$html = '';

	if(strpos($relpath, '/') !== false && !$only_files){

		$history = '<ul class="history">';

		$paths = explode('/', $relpath);

		$nextPath = '';

		$paths = array_filter($paths, function($path){

			return !empty($path); 

		});

		if(sizeof($paths) > 0){

			$history .= sprintf('<li class="history-item"><a href="/%s"><i class="icon-folder-open"></i>/</a></li>', $nextPath);

			end($paths);
			$current = key($paths);
			reset($paths);

			foreach ($paths as $index => $subpath) {

				if($subpath !== ''){
					$nextPath .= $subpath . '/';

					$class = 'history-item';

					if($current == $index)
						$class = 'current';

					$history .= sprintf('<li class="%s"><a href="/%s"><i class="icon-folder-open"></i>%s/</a></li>', $class, $nextPath, $subpath);

				}
				
			}

		} else {

			$history .= sprintf('<li class="current"><a href="/%s"><i class="icon-folder-open"></i>/</a></li>', $nextPath);

		}

		$history .= sprintf('<li><a href="javascript:void(0);">&nbsp;</a></li>', $nextPath);

		$history .= '</ul>';
		$html .= $history;

	}
	$html .= '<ul class="list-files">';

	foreach($folders as $folder){

		$folder_name = $folder;

		if(in_array($folder_name, $skip)) {
			continue;
		} 


		if(!is_dir($path . $folder)){

			$files[] = $folder;
			continue;

		}

		// $folder_name .= '/';

		if($folder_deep === 0){			

			$sub_folders = folders($path . $folder_name, 0, 1, $folder_deep + 1);

			if(!empty(trim($sub_folders))){

				$html .= sprintf('
					<li class="folder">
						<a href="javascript:void(0);" class="favorite"><i class="icon-star-empty"></i></a>
						<a href="%s"><i class="icon-folder"></i>%s</a>
						<span class="open-list icon-doc-text"></span>
						<div class="preview-folders">%s</div>
					</li>
				', $folder, $folder_name, $sub_folders);
				
			} else {

				$html .= sprintf('
					<li>
						<a href="javascript:void(0);" class="favorite"><i class="icon-star-empty"></i></a>
						<a href="%s"><i class="icon-folder-empty"></i>%s</a>
					</li>
				', $folder, $folder_name);
				
			}

		} else {

			$html .= sprintf('
				<li>
					<a href="javascript:void(0);" class="favorite"><i class="icon-star-empty"></i></a>
					<a href="%s"><i class="icon-folder-empty"></i>%s</a>
				</li>
			', $relpath.$folder, $folder_name);

		}

	}


	if($only_folders){

		if (sizeof($files_n_folders) > 0){
			return $html;
		}
		return '';

	}

	$editable_files = array('php', 'js', 'html', 'file', 'txt', 'json', 'sql', 'htaccess', 'css', 'scss', 'ejs', 'py', 'md', 'java', 'class', 're', 'xml');

	foreach ($files as $file) {

		$icon = getIcon($path . '/' . $file);
		$ext = file_get_ext($file);

		$format_args = array($relpath.$file, $icon, $file);	

		$format_string = '
			<li class="file">
				<a href="javascript:void(0);" class="favorite"><i class="icon-star-empty"></i></a>
				<a href="%s">%s%s</a>
			</li>
		';

		if(in_array($ext, $editable_files)){

			array_push($format_args, $relpath.$file);

			$format_string = '
				<li class="file">
					<a href="javascript:void(0);" class="favorite"><i class="icon-star-empty"></i></a>
					<a href="%s">%s%s</a>
					<a class="icon icon-file-code" href="/inspect%s"></a>
				</li>
			';

		}

		$html .= vsprintf($format_string, $format_args);

	}

	$html .= '</ul>';

	return $html;

}

function getIcon($file) {

	$icons = array(
		'php'  => 'file-code',
		'js'   => 'file-code',
		'html' => 'chrome',
		'file' => 'doc',
		'txt'  => 'doc-text',
		'json' => 'doc-text',
		'zip'  => 'file-archive',
		'rar'  => 'file-archive',
		'tar'  => 'file-archive',
		'gz'   => 'file-archive',
		'jpg'  => 'file-image',
		'jpeg' => 'file-image',
		'png'  => 'file-image',
		'gif'  => 'file-image',
		'ppt'  => 'file-powerpoint',
		'doc'  => 'file-word',
		'pdf'  => 'file-pdf',
		'mp3'  => 'file-audio',
		'avi'  => 'file-video',
		'mp4'  => 'file-video',
		'mkv'  => 'file-video',
	);
	$icon = $icons['file'];

	$ext = file_get_ext($file);

	if(isset($icons[$ext])){
		$icon = $icons[$ext];
	}

	return sprintf('<i class="icon-%s"></i>', $icon);


}

function file_get_ext($file){
	$ex_file = explode('.', $file);
	return end($ex_file);
}

function inspector($content){

	preg_match_all($tagPattern, $content, $tags);
	$tagName = isset($tags[1][0])? $tags[1][0]: array();

	preg_match_all($tagClosePattern, $content, $closeTags);
	$tagClose = isset($closeTags[1][0])? $closeTags[1][0]: array();

	preg_match_all($bodyPattern, $content, $bodyHTML);
	$body = isset($bodyHTML[1][0])? $bodyHTML[1][0]: array();

	if(sizeof($tagName) > 0){

		$line = $tagOpen . "<span class=\"tag\">$tagName</span>";

		$tagAttr = isset($tags[2][0])? $tags[2][0]: array();
		if(sizeof($tagAttr) > 0){
			
			$tagAttrs = explode(' ', $tagAttr);
			foreach($tagAttrs as $attr_raw){

				$attr = '';

				$attrInfo = explode('=', $attr_raw, 2);
				$attrName = $attrInfo[0];
				if(empty($attrName))
					continue;

				$attr .= "<span class=\"attr-name\">$attrName</span>";

				if(isset($attrInfo[1])){
					$attrValue = isset($attrInfo[1])? stripslashes($attrInfo[1]): '';
					$attr .= '=';
					$attr .= "<span class=\"attr-val\">$attrValue</span>";

				}

				$line .= " $attr";

			}

		}


		$line .= $tagOpenClose;

		// $content = htmlentities($content);
		// $line .= $tagOpen . $tagName .= $tagClose;
		
		return $line;
	}

	if(sizeof($tagClose) > 0){

	}

	// } else {
		// return htmlentities($content);
	// }

}


function getFileType($file){

	$types = array(
		'js' => 'javascript',
		'html' => 'html',
		'php' => 'php',
		'css' => 'css'
	);

	// $info = pathinfo($file);
	// $type = $info['extension'];

	$type = pathinfo($file, PATHINFO_EXTENSION);

	return isset($types[$type])? $types[$type]: '';
		

}